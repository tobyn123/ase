﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MandelbrotC3443398
{
    public partial class Form1 : Form
    {

        private readonly int MAX = 256;      // max iterations
        private readonly double SX = -2.025; // start value real
        private readonly double SY = -1.125; // start value imaginary
        private readonly double EX = 0.6;    // end value real
        private readonly double EY = 1.125;  // end value imaginary
        private static int x, y, x1, y1, xs, ys, xe, ye, j;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static bool cycling = false;
        private static float xy;
        private Image picture, rectanglepic, combined, imgtarget;
        private Graphics g1, g2, g3, graphics;
        private Cursor c0, c1, c2;
        private Color col;
        




        private HSBColor HSBcol = new HSBColor();
        Pen FractalLine;

        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }
            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;





                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }
        }

 


        public void init() // all instances will be prepared
        {

            this.Height = 550;
            this.Width = 565;
            finished = false;
            c0 = Cursors.Default;
            c1 = Cursors.WaitCursor;
            c2 = Cursors.Cross;
            x1 = Main.Width;
            y1 = Main.Height;
            xy = (float)x1 / (float)y1;
            picture = new Bitmap(x1, y1);
            rectanglepic = new Bitmap(x1, y1);
            combined = new Bitmap(x1,y1);
            g1 = Graphics.FromImage(picture);
            g2 = Graphics.FromImage(rectanglepic);
            g3 = Graphics.FromImage(combined);
            finished = true;
            
            
        }

        private void initvalues() // reset start values
        {
            finished = false;
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;

            finished = true;
        }

        public void start()
        {
            
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
            g1.DrawImage(picture, 0, 0);
            g2.DrawImage(rectanglepic, 0, 0);
            g3.DrawImage(combined, 0, 0);
            Main.Image = combined;
            g3.DrawImage(picture, 0, 0);
            g3.DrawImage(rectanglepic, 0, 0);
            



        }
        public void update(Graphics g)
        {


            if (rectangle)
            {

                g.Clear(Color.Transparent);
                Pen rec = new Pen(Color.White);
                if (xs < xe)
                {

                    if (ys < ye)
                    {
                        g.DrawRectangle(rec, xs, ys, (xe - xs), (ye - ys));
                    }
                    else g.DrawRectangle(rec, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(rec, xe, ys, (xs - xe), (ye - ys));
                    else g.DrawRectangle(rec, xe, ye, (xs - xe), (ys - ye));
                }
                //Refresh();
            }

        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Stream stream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK && (stream = openFileDialog1.OpenFile()) != null) // Test result.
            {
               
                string fileName = openFileDialog1.FileName;
                using (stream)
                {
                    try
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            String line = reader.ReadToEnd();
                            char[] dl = {' '};
                            string[] words = line.Split(dl);

                            try
                            {
                                int.TryParse(words[0], out int tryx);
                                int.TryParse(words[1], out int tryy);
                                double.TryParse(words[2], out double tryxstart);
                                double.TryParse(words[3], out double tryxende);
                                double.TryParse(words[4], out double tryystart);
                                double.TryParse(words[5], out double tryyende);
                                double.TryParse(words[6], out double tryxzoom);
                                double.TryParse(words[7], out double tryyzoom);

                                x = tryx;
                                y = tryy;
                                xstart = tryxstart;
                                xende = tryxende;
                                ystart = tryystart;
                                yende = tryyende;
                                xzoom = tryxzoom;
                                yzoom = tryyzoom;
                            }
                            catch
                            {
                                MessageBox.Show("There was a problem reading the file, please make sure it is in the correct format.",
                                "Error",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning,
                                MessageBoxDefaultButton.Button1);
                            }
                            mandelbrot();
                            finished = false;
                           
                            g3.DrawImage(picture, 0, 0);
                            Main.Refresh();
                        }
                    }
                    catch
                    {
                        MessageBox.Show("There was an error loading the file, please try again.",
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning,
                        MessageBoxDefaultButton.Button2);
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Resize(object sender, EventArgs e)
        {

            this.Main.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void mandelbrot() // calculate all points
        {
            
            float h, b, alt = 0.0f;

            action = false;

            toolStripStatusLabel1.Text = "Mandelbrot-Set will be produced - please wait...";
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y, j); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h;

                           col = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));

                        //djm 
                        alt = h;
                    }

                  
                    FractalLine = new Pen(col);
                    g1.DrawLine(FractalLine,x, y, x + 1, y);



                }
            toolStripStatusLabel1.Text = ("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            
            action = true;
        }

        private float pointcolour(double xwert, double ywert, int j) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }



        public void destroy() // delete all instances 
        {
            if (finished)
            {
                picture = null;
                g1 = null;
                c1 = null;
                c2 = null;
                GC.Collect(); // garbage collection
            }
        }

        private void Main_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            

            if (action)
            {
                cycling = false;
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                g2.Clear(Color.Transparent);
                rectangle = false;
                Refresh();
            }
        }

        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            if (cycling)
            {
                cycling = false;
                action = true;
            }

            if (action)
            {
                xs = e.X;
                ys = e.Y;
                rectangle = true;
                

            }
        }

        private void Main_Paint(object sender, PaintEventArgs e)
        {

            if (cycling)
            {
                g3.DrawImage(imgtarget, 0, 0);
                g3.DrawImage(rectanglepic, 0, 0);
                
            }
            else
            {

                g3.DrawImage(picture, 0, 0);
                g3.DrawImage(rectanglepic, 0, 0);
                
            }
            GC.Collect();

        }

        private void Main_MouseMove(object sender, MouseEventArgs e)
        {   
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                update(g2);
                Refresh();
                
            }

            
        }

        private void Main_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "DefaultOutputName.txt";
            save.Filter = "Text File | *.txt";
            if (save.ShowDialog() == DialogResult.OK)
            {

                string path = save.FileName;
                StreamWriter writer = new StreamWriter(File.Create(path));
                writer.Write(x + " " + y + " " + xstart + " " + xende + " " + ystart + " " + yende + " " + xzoom + " " + yzoom);
                writer.Dispose();
                writer.Close();

            }
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e) //NOT WORKING AS SHOULD
        {
            action = false;
            finished = true;
            g3.Clear(Color.White);
        }

        private void exportImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.Filter = "Bitmap files (*.bmp)|*.bmp|JPG files (*.jpg)|*.jpg|GIF files (*.gif)|*.gif|PNG files (*.png)|*.png|TIF files (*.tif)|*.tif|All files (*.*)|*.*";
            save.FilterIndex = 1;
            save.AddExtension = true;
            save.RestoreDirectory = true;
            save.OverwritePrompt = true;
            

            if ((save.ShowDialog() == DialogResult.OK))
            { 
                if (Path.GetExtension(save.FileName).ToLower() == ".bmp")
                {
                    Main.Image.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                }
                    else if (Path.GetExtension(save.FileName).ToLower() == ".jpg")
                {
                    Main.Image.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                    else if (Path.GetExtension(save.FileName).ToLower() == ".gif")
                {
                    Main.Image.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Gif);
                }
                    else if (Path.GetExtension(save.FileName).ToLower() == ".png")
                {
                    Main.Image.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Png);
                }
                    else if (Path.GetExtension(save.FileName).ToLower() == ".tif")
                {
                    Main.Image.Save(save.FileName, System.Drawing.Imaging.ImageFormat.Tiff);
                }
                else
                {
                    MessageBox.Show("File Save Error.");
                }

            }
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 50;
            mandelbrot();


        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 95;
            mandelbrot();

        }

        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 130;
            mandelbrot();

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if (cycling)
            {
            action = false;

            Bitmap mand = (Bitmap)(picture);

            imgtarget = mand.Clone(new Rectangle(0, 0, mand.Width, mand.Height), PixelFormat.Format8bppIndexed);
            ColorPalette palette = imgtarget.Palette;

            Random rnd = new Random();

            for (int i = 1; i < palette.Entries.Length - 1; i++)
            {
                palette.Entries[i] = Color.FromArgb(rnd.Next(1, 255), rnd.Next(1, 255), rnd.Next(1, 255));
            }

            cycling = true;
            imgtarget.Palette = palette;
            mand = new Bitmap(imgtarget.Width, imgtarget.Height);
            graphics = Graphics.FromImage(mand);
            Refresh();
            }
            
            

        }

        private void onToolStripMenuItem_Click(object sender, EventArgs e)
        {
            action = false;
            cycling = true;
            timer1.Start();
        }

        private void offToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cycling = false;
            timer1.Stop();
            action = true;
            Refresh();
        }

        private void colourCyclingToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            j = 0;
            init();
            start();
        }

        private void Main_MouseEnter(object sender, EventArgs e)
        {
            Cursor = c2;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001");
        }


    }
}
